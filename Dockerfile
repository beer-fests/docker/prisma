FROM node:10.13.0-alpine

RUN npm install --global prisma@1.20.1 yarn@1.12.3

RUN apk --update add git openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

CMD ["/bin/sh"]
